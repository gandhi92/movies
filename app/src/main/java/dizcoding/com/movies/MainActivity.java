package dizcoding.com.movies;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import dizcoding.com.movies.data.MoviesData;

public class MainActivity extends AppCompatActivity {
    RecyclerView rv_movie;
    RecyclerView.Adapter rvAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        defineView();
        showData();
    }

    private void showData() {
        MoviesData moviesData = new MoviesData(this,rvAdapter,rv_movie);
        moviesData.getData();
    }

    private void defineView() {
        rv_movie = findViewById(R.id.rv_movies);
        rv_movie.setHasFixedSize(true);
        RecyclerView.LayoutManager lm_movie = new LinearLayoutManager(this);
        rv_movie.setLayoutManager(lm_movie);
    }
}
