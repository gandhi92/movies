package dizcoding.com.movies.data;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;

import org.json.JSONObject;

import dizcoding.com.movies.adapter.AdapterMovie;
import dizcoding.com.movies.parser.MoviesParser;
import okhttp3.Response;

public class MoviesData {
    Context context;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;

    public MoviesData(Context context, RecyclerView.Adapter adapter, RecyclerView rv_movie){
        this.context = context;
        this.adapter = adapter;
        this.recyclerView = rv_movie;
    }

    public void getData(){
        AndroidNetworking.initialize(context);
        AndroidNetworking.get("https://api.themoviedb.org/3/discover/movie?api_key={apikey}&language=en-US")
                .setPriority(Priority.LOW)
                .build()
                .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                    @Override
                    public void onResponse(Response okHttpResponse, JSONObject response) {
                        MoviesParser moviesParser = new MoviesParser();

                        adapter = new AdapterMovie(context,moviesParser.data(response.toString()));
                        adapter.notifyDataSetChanged();
                        recyclerView.setAdapter(adapter);
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}
