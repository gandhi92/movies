package dizcoding.com.movies.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import dizcoding.com.movies.models.ModelMovies;

public class MoviesParser {

    public ArrayList<ModelMovies> data(String jsonstring){

        ArrayList<ModelMovies> modelMoviesArrayList = new ArrayList<>();
        try {
            JSONObject json = new JSONObject(jsonstring);
            JSONArray results = json.getJSONArray("results");
            for (int i = 0; i< results.length(); i++){
                JSONObject jsonObject = results.getJSONObject(i);
                String title = jsonObject.getString("title");
                String poster_path = jsonObject.getString("poster_path");
                String release_date = jsonObject.getString("release_date");
                String overview = jsonObject.getString("overview");

                ModelMovies modelMovies = new ModelMovies();
                modelMovies.setTitle(title);
                modelMovies.setPoster_path(poster_path);
                modelMovies.setRelease_date(release_date);
                modelMovies.setOverview(overview);
                modelMoviesArrayList.add(modelMovies);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return modelMoviesArrayList;
    }
}
