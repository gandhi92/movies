package dizcoding.com.movies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailMovieActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);

        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        String poster_path = intent.getStringExtra("poster_path");
        String release = intent.getStringExtra("release");
        String overview = intent.getStringExtra("overview");

        ImageView gambar = findViewById(R.id.detail_gambar);
        Glide.with(this).load("https://image.tmdb.org/t/p/w342/"+poster_path).into(gambar);

        TextView detail_title = findViewById(R.id.detail_title);
        detail_title.setText(title);
        TextView detail_release = findViewById(R.id.detail_release);
        detail_release.setText(release);
        TextView detail_overview= findViewById(R.id.detail_overview);
        detail_overview.setText(overview);
    }
}
